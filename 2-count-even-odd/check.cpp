#include <iostream>

using namespace std;

// This program does not get built by the Makefile, so if you type make, it will not build it for you

// You need to do it manually: g++ check.cpp -o jerry

// Instead of jerry, you can call it whatever you want, and that will be the exectuable it produces

int main(){

    int c = 0;

    for (int i = 0; i < 10; i++){ // We know the file has 10 lines
        int age;
        cin >> age;
        
        if (age >= 18){ // Check if age is 18 or older
            c++;
        }
    }

    cout << "There are " << c << " people who can enter" << endl; // Report your result

    return 0;
}